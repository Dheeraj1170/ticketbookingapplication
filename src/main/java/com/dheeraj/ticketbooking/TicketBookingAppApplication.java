package com.dheeraj.ticketbooking;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.dheeraj.ticketbooking.model.Ticket;
import com.dheeraj.ticketbooking.service.TicketService;

@SpringBootApplication
public class TicketBookingAppApplication implements CommandLineRunner {

	@Autowired
	private TicketService tckServiceObj;
	
	public static void main(String[] args) {
		SpringApplication.run(TicketBookingAppApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	 
		Ticket ticketObj = new Ticket();
		ticketObj.setPassengerName("Dheeraj");
		ticketObj.setSourceName("Dhone");
		ticketObj.setDestinationName("Pune");
		ticketObj.setTravelDate(new Date());
		ticketObj.setEmail("abc@gmail.com");
		
		tckServiceObj.createTicket(ticketObj);
		
	}

}
